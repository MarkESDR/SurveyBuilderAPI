"""
Serializers for use in the Survey API
"""
from rest_framework import serializers
from surveys.models import Survey, SurveyElement

class ElementSerializer(serializers.ModelSerializer):
    """
    Defines a serializer for the Survey Element modal
    """
    class Meta:
        model = SurveyElement
        fields = ('id', 'rank', 'title', 'label', 'element_type_id', 'element_type_name', 'survey')


class SurveySerializer(serializers.ModelSerializer):
    """
    Defines a serializer for the Survey model
    """
    elements = ElementSerializer(many=True, read_only=True)

    class Meta:
        model = Survey
        fields = ('id', 'created', 'title', 'elements')
