from rest_framework import generics
from surveys.models import Survey, SurveyElement
from surveys.serializers import SurveySerializer, ElementSerializer

class SurveyList(generics.ListCreateAPIView):
    """
    List all surveys
    """
    queryset = Survey.objects.all()
    serializer_class = SurveySerializer

class SurveyDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update, or destroy an Survey
    """
    queryset = Survey.objects.all()
    serializer_class = SurveySerializer

class ElementList(generics.ListCreateAPIView):
    """
    List all survey elements
    """
    queryset = SurveyElement.objects.all()
    serializer_class = ElementSerializer