"""
Define url paths for a survey
"""
from django.conf.urls import url
from surveys import views

urlpatterns = [
    url(r'^surveys?/(?P<pk>[0-9]+)/?$', views.SurveyDetail.as_view()),
    url(r'^surveys?/?$', views.SurveyList.as_view()),
    url(r'^elements?/?$', views.ElementList.as_view()),
]
