# Generated by Django 2.0.5 on 2018-05-15 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('surveys', '0004_remove_surveyelement_element_type_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='surveyelement',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
