"""
Define models for use in Survey API
"""
import uuid
from django.db import models


class Survey(models.Model):
    """
    Defines a survey model
    """
    created = models.DateTimeField(auto_now_add=True, editable=False)
    title = models.CharField(max_length=100)


def create_short_uuid():
    """Return string of first 8 chars of a generated uuid"""
    return str(uuid.uuid4())[:8]

class SurveyElement(models.Model):
    """
    Defines a survey element model
    """
    ELEMENT_TYPES = (
        (1, 'Check Box'),
        (2, 'Dropdown Options'),
        (3, 'Paragraph Box'),
        (4, 'Radio Options'),
        (5, 'Short Text Box'),
    )
    id = models.CharField(max_length=8, default=create_short_uuid, primary_key=True)
    rank = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=100)
    label = models.TextField()
    element_type_id = models.IntegerField(choices=ELEMENT_TYPES, blank=False)
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE, related_name='elements')

    @property
    def element_type_name(self):
        return self.get_element_type_id_display()
